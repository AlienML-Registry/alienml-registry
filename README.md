# AlienML Registry

The purpose of this git is only to host the AlienML docker images.

To be able to push to the registry, you first need to be a member of the repo:
- Login with the owner account (AlienML-Registry)
- Go to Manage -> Members -> Invite member
- Search your account, and add it to the project
- You can now push to the registry
